package com.rpg.packages;

import com.comphenix.protocol.ProtocolLibrary;
import com.comphenix.protocol.ProtocolManager;
import com.comphenix.protocol.events.PacketContainer;
import com.rpg.RPGCore;
import com.rpg.util.colorutil.ChatColorUtil;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.entity.*;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.metadata.FixedMetadataValue;
import org.bukkit.scheduler.BukkitScheduler;

import java.util.*;

public class DamageIndicator {
    private static ProtocolManager protocolManager =  ProtocolLibrary.getProtocolManager();

    public static Map<UUID,Entity> damageIndicatorsEntities = new HashMap<>();

    @Deprecated
    public  void displayDamageIndicator(EntityDamageByEntityEvent damageEvent , double damage, Player player){

        Entity entity = damageEvent.getEntity();
        Location location = entity.getLocation();

        Random random = new Random();
        double start = -1.3;
        double end = 1.3;
        double varX = start + (random.nextDouble() * (end - start));
        double varZ = start + (random.nextDouble() * (end - start));

       // int entityId = (int)(Math.random() * Integer.MAX_VALUE);

        location.setX( location.getX() + varX);
        location.setY(location.getY()-0.3);
        location.setZ(location.getZ() +varZ);

//
//
//        PacketContainer newPacket = protocolManager.createPacket(PacketType.Play.Server.SPAWN_ENTITY);
//        newPacket.getIntegers().write(0,entityId);
//        newPacket.getEntityTypeModifier().write(0, EntityType.ARMOR_STAND);
//
//        newPacket.getDoubles().write(0, location.getX());
//        newPacket.getDoubles().write(1, location.getY());
//        newPacket.getDoubles().write(2, location.getZ());

        float lastDamage = (float) damage;
        buildArmorStand( entity, location, ChatColorUtil.boldText("-"+lastDamage,ChatColor.RED), null, player);

//        BukkitScheduler scheduler =  Bukkit.getServer().getScheduler();
//        scheduler.scheduleSyncDelayedTask(RPGCore.getPlugin(),() ->{
//            PacketContainer destroyPacket = new PacketContainer(PacketType.Play.Server.ENTITY_DESTROY);
//
//            destroyPacket.getIntegerArrays().write(0, new int[]{entityId});
//
//            try {
//                protocolManager.sendServerPacket(player, destroyPacket);
//            } catch (InvocationTargetException e) {
//                Bukkit.broadcastMessage(e.getMessage());
//                e.printStackTrace();
//            }
//        } , 40);
    }

    private void buildArmorStand(Entity entity,Location location , String damage, PacketContainer newPacket, Player player){

        BukkitScheduler scheduler =  Bukkit.getServer().getScheduler();

        World world = entity.getWorld();
        ArmorStand armorStand = (ArmorStand) world.spawnEntity( location, EntityType.ARMOR_STAND);
        armorStand.setInvulnerable(true);
        armorStand.setCollidable(false);
        armorStand.setMetadata("DAMAGE_INDICATOR", new FixedMetadataValue(RPGCore.getPlugin(),"DAMAGE_INDICATOR"));
        armorStand.setGravity(false);
        armorStand.setVisible(false);
        armorStand.setRemoveWhenFarAway(true);
        armorStand.setCollidable(false);

        armorStand.setCustomNameVisible(true);
        armorStand.setCustomName(damage);

        damageIndicatorsEntities.put(armorStand.getUniqueId(),armorStand);

        scheduler.runTaskTimer(RPGCore.getPlugin(), () -> {
            Location loc = armorStand.getLocation();
            loc.setY(loc.getY() -0.1);
            armorStand.teleport(loc);
        } ,0 , 1);

        scheduler.scheduleSyncDelayedTask(RPGCore.getPlugin(),() ->{
            armorStand.remove();
            damageIndicatorsEntities.remove(armorStand.getUniqueId());
        } , 40);
    }

}
