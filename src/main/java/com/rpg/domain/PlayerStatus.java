package com.rpg.domain;

public class PlayerStatus {

    private String playerUUID;
    private String playerName;
    private Double currentXp;
    private Double maxXp;
    private Integer level;
    private Double balance;
    private Integer core;
    private Double health;
    private Double maxHealth;
    private Double mana;
    private Double maxMana;

    public String getPlayerUUID() {
        return playerUUID;
    }

    public void setPlayerUUID(String playerUUID) {
        this.playerUUID = playerUUID;
    }

    public String getPlayerName() {
        return playerName;
    }

    public void setPlayerName(String playerName) {
        this.playerName = playerName;
    }

    public Double getCurrentXp() {
        return currentXp;
    }

    public void setCurrentXp(Double currentXp) {
        this.currentXp = currentXp;
    }

    public Double getMaxXp() {
        return maxXp;
    }

    public void setMaxXp(Double maxXp) {
        this.maxXp = maxXp;
    }

    public Integer getLevel() {
        return level;
    }

    public void setLevel(Integer level) {
        this.level = level;
    }

    public Double getBalance() {
        return balance;
    }

    public void setBalance(Double balance) {
        this.balance = balance;
    }

    public Integer getCore() {
        return core;
    }

    public void setCore(Integer core) {
        this.core = core;
    }

    public Double getHealth() {
        return health;
    }

    public void setHealth(Double health) {
        this.health = health;
    }

    public Double getMaxHealth() {
        return maxHealth;
    }

    public void setMaxHealth(Double maxHealth) {
        this.maxHealth = maxHealth;
    }

    public Double getMana() {
        return mana;
    }

    public void setMana(Double mana) {
        this.mana = mana;
    }

    public Double getMaxMana() {
        return maxMana;
    }

    public void setMaxMana(Double maxMana) {
        this.maxMana = maxMana;
    }

    @Override
    public String toString() {
        return "PlayerStatus{" +
                "playerUUID='" + playerUUID + '\'' +
                ", playerName='" + playerName + '\'' +
                ", currentXp=" + currentXp +
                ", maxXp=" + maxXp +
                ", level=" + level +
                ", balance=" + balance +
                ", core=" + core +
                '}';
    }
}
