package com.rpg.service;

import com.rpg.RPGCore;
import com.rpg.packages.DamageIndicator;
import com.rpg.repository.impl.PlayerCheckinImpl;
import com.rpg.util.TranslateUtil;
import com.rpg.util.colorutil.ChatColorUtil;
import com.rpg.util.config.ConfigPlayers;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.entity.Arrow;
import org.bukkit.entity.Player;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.event.player.PlayerRespawnEvent;
import org.bukkit.plugin.Plugin;

import java.util.Random;

public class PlayerDamageService {

    private static ConfigPlayers configPlayers = ConfigPlayers.getInstance();
    private static FileConfiguration configLeveling = RPGCore.getConfigLeveling().getConfig();
    private  static  FileConfiguration configuration = RPGCore.getConfigRPG().getConfig();

    private static Plugin plugin =  RPGCore.getPlugin();

    private static int MAX_LEVEL = configLeveling.getInt("Config.MAX_LEVEL");

    private static boolean LIFE_SCALE_ENABLE = configLeveling.getBoolean("Config.LIFE_SCALE_ENABLE");
    private static int ROLL_CHANCE = configuration.getInt("Config.ROLL_CHANCE");
    private static int DODGE_CHANCE = configuration.getInt("Config.DODGE_CHANCE");
    private static int MAX_FALL_DAMAGE_REDUCER = configuration.getInt("Config.MAX_FALL_DAMAGE_REDUCER");

    @Deprecated
    public static void onPlayerTakeDamage( Double damage, EntityDamageEvent event){

        if(!(event.getEntity() instanceof Player))
            return;

        Player player = ((Player) event.getEntity()).getPlayer();
        if (!PlayerCheckinService.getPlayerCheckin(player))
            return;

        if(!LIFE_SCALE_ENABLE)
            return;

        double maxHealth =  configPlayers.getConfiguration(player).getInt("maxHealth");
        double newDamage = (damage * player.getMaxHealth())/maxHealth;

        ConfigurationSection playerSection = configPlayers.getConfiguration(player);
        ConfigurationSection attributeSection = playerSection.getConfigurationSection("Attributes");

        int constitution =  attributeSection.getInt("constitution");
        int dexterity =  attributeSection.getInt("dexterity");
        Random random = new Random();


        switch (event.getCause()){
            case FALL:
                int minimalChance = ROLL_CHANCE + (dexterity/2);

                if(random.nextInt(99) + 1 <= minimalChance){
                    ActionBarService.buildNoticeStatus(player, ChatColorUtil.boldText("  ▒▒▒ "+ TranslateUtil.getKey("ROLL") +" ▒▒▒  ",ChatColor.GREEN));
                    if(event.getDamage() > MAX_FALL_DAMAGE_REDUCER) {
                        newDamage = newDamage - MAX_FALL_DAMAGE_REDUCER;
                    }else{
                     event.setCancelled(true);
                     return;
                    }
                }
            break;
            case ENTITY_ATTACK :
                dodgeDamageChance(DODGE_CHANCE + (dexterity/2), player ,  event);
            break;
            case ENTITY_SWEEP_ATTACK:
                dodgeDamageChance(DODGE_CHANCE + (dexterity/2), player ,  event);
                break;
            case PROJECTILE:
                dodgeDamageChance(DODGE_CHANCE + (dexterity/2), player ,  event);
                break;
            case POISON:
                float resistancePoison = (float) (constitution / MAX_LEVEL);
                newDamage = newDamage - resistancePoison ;
            break;
        }

        event.setDamage(newDamage);

        plugin.getServer().getScheduler().runTaskLater(plugin, () -> {
            configPlayers.getConfiguration(player).set("health",(int) Math.ceil(((player.getHealth() * maxHealth)/player.getMaxHealth())));
            configPlayers.saveChanges();
        }, 0);
    }



    private static void dodgeDamageChance(int minimalChanceDodge, Player player, EntityDamageEvent event){
        if(new Random().nextInt(99) + 1 <= minimalChanceDodge){
            ActionBarService.buildNoticeStatus(player, ChatColorUtil.boldText("  ▒▒▒ "+ TranslateUtil.getKey("DODGE") +" ▒▒▒  ",ChatColor.GREEN));
            event.setCancelled(true);
        }
    }

    @Deprecated
    public static void onPlayerDie(PlayerRespawnEvent event){

        if (!PlayerCheckinService.getPlayerCheckin(event.getPlayer()))
            return;

        if(!LIFE_SCALE_ENABLE)
            return;

        double maxHealth =  configPlayers.getConfiguration(event.getPlayer()).getInt("maxHealth");
        configPlayers.getConfiguration(event.getPlayer()).set("health",maxHealth);
        configPlayers.saveChanges();

        LoaderPlayerStatusService.createScoreBoardAutoUpdate(event.getPlayer());
    }

    public static void onPlayerHit(EntityDamageByEntityEvent event){

        Player player = null;

            if(event.getEntity().hasMetadata("DAMAGE_INDICATOR")){
                event.setCancelled(true);
                return;
            }

            if((event.getDamager() instanceof  Player))
                player = (Player) event.getDamager();

            if(player == null && event.getDamager() instanceof Arrow  && ((Arrow) event.getDamager()).getShooter() instanceof  Player)
                player = (Player) ((Arrow) event.getDamager()).getShooter();

            if ( player == null || !PlayerCheckinService.getPlayerCheckin(player))
                return;

            ConfigurationSection playerSection = configPlayers.getConfiguration(player);
            ConfigurationSection attributeSection = playerSection.getConfigurationSection("Attributes");

            int strength =  attributeSection.getInt("strength");
            int dexterity =  attributeSection.getInt("dexterity");

            switch (event.getCause()){
                case ENTITY_ATTACK:
                case ENTITY_SWEEP_ATTACK:
                    event.setDamage(event.getDamage()+(strength/2));
                    entityDamage(event,event.getDamage(), player);
                    break;
                case PROJECTILE:
                    event.setDamage(event.getDamage()+(dexterity/2));
                    entityDamage(event, event.getDamage(), player);
                    break;
                default:
                    break;
            }

    }

    @Deprecated
    private  static void entityDamage(EntityDamageByEntityEvent event, double damage, Player player){
        new DamageIndicator().displayDamageIndicator(event, damage, player);
    }


}
