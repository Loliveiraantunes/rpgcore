package com.rpg.service;

import com.rpg.RPGCore;
import com.rpg.util.config.ConfigPlayers;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.entity.Player;

public class PlayerMoveService {


    private static ConfigPlayers configPlayers = ConfigPlayers.getInstance();
    private static FileConfiguration configLeveling = RPGCore.getConfigLeveling().getConfig();
    private static int MAX_LEVEL = configLeveling.getInt("Config.MAX_LEVEL");

    public static void onPlayerMove(Player player){

        if(!PlayerCheckinService.getPlayerCheckin(player))
            return;

        ConfigurationSection playerSection = configPlayers.getConfiguration(player);
        ConfigurationSection attributeSection = playerSection.getConfigurationSection("Attributes");

        int dexterity =  attributeSection.getInt("dexterity");

        float movimentSpeed = (float) dexterity/MAX_LEVEL;
        if(movimentSpeed < 0.12){
            player.setWalkSpeed((float) (0.2+(movimentSpeed/2)));
        }else{
            player.setWalkSpeed((float) 0.32);
        }

    }
}
