package com.rpg.service;

import com.rpg.domain.PlayerStatus;
import com.rpg.repository.impl.PlayerStatusImpl;
import com.rpg.repository.interfaces.PlayerStatusRepository;
import com.rpg.util.TranslateUtil;
import com.rpg.util.colorutil.ChatColorUtil;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import org.bukkit.scoreboard.DisplaySlot;
import org.bukkit.scoreboard.Objective;
import org.bukkit.scoreboard.Scoreboard;

public class LoaderPlayerStatusService {

    public static PlayerStatusRepository playerStatusRepository = new PlayerStatusImpl();

    @Deprecated
    public static void createScoreBoardAutoUpdate(Player player){
        if(!PlayerCheckinService.getPlayerCheckin(player)){
            Scoreboard scoreboard = Bukkit.getScoreboardManager().getNewScoreboard();
            player.setScoreboard(scoreboard);
            return;
        }

        PlayerStatus playerStatus = playerStatusRepository.getPlayerStatus(player);
        //Os item que possuem o mesmo valor na mesma linha acabam não aparecendo, eu apliquei cores diferentes quando o registro é igual e aí funcionou.
        Scoreboard scoreboard = Bukkit.getScoreboardManager().getNewScoreboard();
        Objective objective = scoreboard.registerNewObjective("dummy","title");
        objective.setDisplaySlot(DisplaySlot.SIDEBAR);
        objective.setDisplayName(ChatColorUtil.boldText("    "+playerStatus.getPlayerName()+"     ",ChatColor.GOLD));
        objective.getScore(ChatColorUtil.boldText(" "+TranslateUtil.getKey("CLASS")+": "+ ChatColorUtil.textColor(TranslateUtil.getKey("NO_CLASS"),ChatColor.WHITE),ChatColor.GREEN)).setScore(50);
        objective.getScore(ChatColor.GREEN+"-------------------").setScore(41);
        objective.getScore(ChatColorUtil.boldText(" "+TranslateUtil.getKey("LEVEL")+":          "+ChatColor.AQUA+playerStatus.getLevel().toString(),ChatColor.GREEN)).setScore(40);
        objective.getScore(ChatColor.GREEN+"------------------- ").setScore(31);
        objective.getScore(ChatColorUtil.boldText(" XP:   ",ChatColor.GREEN)).setScore(30);
        int percent = (int) ((playerStatus.getCurrentXp()*100)/playerStatus.getMaxXp());
        objective.getScore("    "+ChatColor.GRAY+ (int)((double)playerStatus.getCurrentXp())+" / "+ ChatColor.YELLOW+ (int)((double)playerStatus.getMaxXp()) + ChatColor.AQUA+"  ("+percent+ "%)").setScore(20);

        if(playerStatus.getCore() > 0){
            objective.getScore(ChatColor.YELLOW+"-------------------").setScore(12);
            objective.getScore(ChatColorUtil.boldText(" Core: "+playerStatus.getCore(), ChatColor.YELLOW)).setScore(11);
        }

        objective.getScore(ChatColor.GOLD+"-------------------").setScore(1);
        objective.getScore(ChatColorUtil.boldText(" "+TranslateUtil.getKey("GOLD")+":  "+ChatColor.GRAY+playerStatus.getBalance(),ChatColor.GOLD)).setScore(0);
        player.setScoreboard(scoreboard);
    }


}
