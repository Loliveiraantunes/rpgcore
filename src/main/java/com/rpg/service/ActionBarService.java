package com.rpg.service;

import com.rpg.RPGCore;
import com.rpg.util.ProtocolUtil;
import com.rpg.util.colorutil.ChatColorUtil;
import com.rpg.util.config.ConfigPlayers;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Sound;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Player;
import org.bukkit.event.entity.EntityRegainHealthEvent;
import org.bukkit.plugin.Plugin;

import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

public class ActionBarService {

    private static ConfigPlayers configPlayers = ConfigPlayers.getInstance();
    private static Plugin plugin =  RPGCore.getPlugin();

    private static  Map<UUID,String > noticeStatus = new HashMap<>();

    private static FileConfiguration configLeveling = RPGCore.getConfigLeveling().getConfig();
    private static boolean LIFE_SCALE_ENABLE = configLeveling.getBoolean("Config.LIFE_SCALE_ENABLE");

    @Deprecated
    public static void updateActionBar(Entity entity){

        if(!PlayerCheckinService.getPlayerCheckin(entity))
            return;

        Player player = (Player) entity;
        StringBuilder displayText = new StringBuilder();
        String notice = noticeStatus.get(player.getUniqueId());
        buildHealthStatus(displayText,player);
        displayText.append( notice != null ? notice : "                    ");
        buildManaStatus(displayText,player);
        ProtocolUtil.displayMessage(player,displayText.toString() );
    }

    @Deprecated
    private static void buildHealthStatus(StringBuilder displayText, Player player){
        int health = configPlayers.getConfiguration(player).getInt("health");
        int maxHealth = configPlayers.getConfiguration(player).getInt("maxHealth");

        displayText.append(ChatColorUtil.textColor("❤ "+health+" ︱ "+maxHealth, ChatColor.RED));
    }

    public static void buildNoticeStatus(Player player, String message){
         noticeStatus.put(player.getUniqueId(), message);
         player.playSound(player.getLocation(), Sound.ENTITY_FIREWORK_ROCKET_LAUNCH, 1F, 1F);
         plugin.getServer().getScheduler().runTaskLaterAsynchronously(plugin, () -> noticeStatus.put(player.getUniqueId(),"                    "), 1*60);
    }

    private static void buildManaStatus(StringBuilder displayText, Player player){
        int health = configPlayers.getConfiguration(player).getInt("mana");
        int maxHealth = configPlayers.getConfiguration(player).getInt("maxMana");

        displayText.append(ChatColorUtil.textColor("✦ "+health+" ︱ "+maxHealth, ChatColor.AQUA));
    }

    @Deprecated
    public static void onPlayerRegainHealth(EntityRegainHealthEvent event){
        if(!PlayerCheckinService.getPlayerCheckin(event.getEntity()))
            return;

        if(!LIFE_SCALE_ENABLE)
            return;

        Player player = (Player) event.getEntity();
        double maxHealth =  configPlayers.getConfiguration(player).getInt("maxHealth");

        ConfigurationSection playerSection = configPlayers.getConfiguration(player);
        ConfigurationSection attributeSection = playerSection.getConfigurationSection("Attributes");
        int constitution =  attributeSection.getInt("constitution");

        event.setAmount((float)((1*player.getMaxHealth())+constitution)/maxHealth);

        configPlayers.getConfiguration(player).set("health",(float)(player.getHealth() * maxHealth) / (player.getMaxHealth()));

        if((player.getHealth() + event.getAmount()) >= player.getMaxHealth())
            configPlayers.getConfiguration(player).set("health",maxHealth);

        configPlayers.saveChanges();
    }


}
