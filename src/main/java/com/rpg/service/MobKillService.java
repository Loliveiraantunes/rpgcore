package com.rpg.service;

import com.rpg.RPGCore;
import com.rpg.repository.impl.PlayerStatusImpl;
import com.rpg.repository.interfaces.PlayerStatusRepository;
import org.bukkit.entity.Arrow;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.entity.EntityDeathEvent;
import org.bukkit.plugin.Plugin;

public class MobKillService {

    private static Plugin plugin = RPGCore.getPlugin();
    private PlayerStatusRepository playerStatusRepository = new PlayerStatusImpl();


    public void MobKillEvent(EntityDeathEvent event) {
        if(event.getEntity().getKiller() instanceof Player){
            Player player = event.getEntity().getKiller();
            if(!PlayerCheckinService.getPlayerCheckin(player))
                return;
            playerStatusRepository.setExperience(player, event.getEntity());
        }

    }

}
