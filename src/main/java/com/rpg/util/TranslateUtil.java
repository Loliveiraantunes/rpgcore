package com.rpg.util;

import com.rpg.RPGCore;
import org.bukkit.Bukkit;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.entity.Player;
import org.bukkit.plugin.Plugin;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.Enumeration;
import java.util.Locale;
import java.util.Properties;

public class TranslateUtil {
    private static final String LOCAL_LANGUAGE = RPGCore.getConfigTranslateFiles().getConfig().getString("local-language").toUpperCase();

    public static String getKey(String key){
        return RPGCore.getConfigTranslateFiles().getMessage(LOCAL_LANGUAGE, key.toLowerCase());
    }

    public static String getKey(String key, Player player){
        return RPGCore.getConfigTranslateFiles().getMessage(LOCAL_LANGUAGE, key.toLowerCase()).replace("{$playerName}",player.getName());
    }
}
