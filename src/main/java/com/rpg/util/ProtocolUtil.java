package com.rpg.util;

import com.comphenix.protocol.PacketType;
import com.comphenix.protocol.ProtocolLibrary;
import com.comphenix.protocol.ProtocolManager;
import com.comphenix.protocol.events.PacketContainer;
import com.comphenix.protocol.wrappers.EnumWrappers;
import com.comphenix.protocol.wrappers.WrappedChatComponent;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;

import java.lang.reflect.InvocationTargetException;

public class ProtocolUtil {

    private static ProtocolManager protocolManager =  ProtocolLibrary.getProtocolManager();

    public static void displayMessage(Player player, String msg){

        try {
            PacketContainer packet = protocolManager.createPacket(PacketType.Play.Server.TITLE);
            packet.getEnumModifier(EnumWrappers.TitleAction.class, 0).write(0, EnumWrappers.TitleAction.ACTIONBAR);
            packet.getChatComponents().write(0, WrappedChatComponent.fromText(msg));

            protocolManager.sendServerPacket(player, packet);

        } catch (InvocationTargetException e) {
            Bukkit.getLogger().info("ProtocolLib Error "+e.getMessage());
        }
    }

}
